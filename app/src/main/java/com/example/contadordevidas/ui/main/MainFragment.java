package com.example.contadordevidas.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.contadordevidas.R;


public class MainFragment extends Fragment {

    private TextView contador1;
    private TextView contador2;

    private int life1;
    private int life2;
    private int posion1;
    private int posion2;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if(savedInstanceState !=null){
            life1=savedInstanceState.getInt("vida1");
            life2=savedInstanceState.getInt("vida2");
            posion1=savedInstanceState.getInt("poison1");
            posion2=savedInstanceState.getInt("poison2");
        } else {
            reset();
        }
        final View view = inflater.inflate(R.layout.main_fragment, container, false);

        Button p1minus = view.findViewById(R.id.p1minus);
        Button p1plus = view.findViewById(R.id.p1plus);
        Button p2plus = view.findViewById(R.id.p2plus);
        Button p2minus = view.findViewById(R.id.p2minus);
        ImageButton arrowUp = view.findViewById(R.id.arrowUp);
        ImageButton arrowDown = view.findViewById(R.id.ArrowDown);
        ImageButton h1plus = view.findViewById(R.id.h1plus);
        ImageButton h1minus = view.findViewById(R.id.h1minus);
        ImageButton h2plus = view.findViewById(R.id.h2plus);
        ImageButton h2minus = view.findViewById(R.id.h2minus);
        contador1 = view.findViewById(R.id.contador1);
        contador2 = view.findViewById(R.id.contador2);

        View.OnClickListener listener = new View.OnClickListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.ArrowDown:
                        life1--;
                        life2++;
                        break;
                    case R.id.arrowUp:
                        life1++;
                        life2--;
                        break;

                    case R.id.p1plus:
                        posion1++;
                        break;

                    case R.id.p1minus:
                        posion1--;
                        break;

                    case R.id.p2plus:
                        posion2++;
                        break;

                    case R.id.p2minus:
                        posion2--;
                        break;

                    case R.id.h1plus:
                        life1++;
                        break;

                    case R.id.h1minus:
                        life1--;
                        break;

                    case R.id.h2plus:
                        life2++;
                        break;

                    case R.id.h2minus:
                        life2--;
                        break;
                }
                updateView();
            }
        };

        updateView();


        // Configurando el listener
        p1minus.setOnClickListener(listener);
        p1plus.setOnClickListener(listener);
        p2plus.setOnClickListener(listener);
        p2minus.setOnClickListener(listener);
        arrowUp.setOnClickListener(listener);
        arrowDown.setOnClickListener(listener);
        h1plus.setOnClickListener(listener);
        h1minus.setOnClickListener(listener);
        h2plus.setOnClickListener(listener);
        h2minus.setOnClickListener(listener);
        contador1.setOnClickListener(listener);
        contador2.setOnClickListener(listener);
        //



        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("vida1",life1);
        outState.putInt("vida2",life2);
        outState.putInt("poison1",posion1);
        outState.putInt("poison2",posion2);
        super.onSaveInstanceState(outState);
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    private void reset(){
        posion1=0;
        posion2=0;
        life1=25;
        life2=25;
    }

    @SuppressLint("DefaultLocale")
    private void updateView(){
        contador1.setText(String.format("%d/%d",life1,posion1));
        contador2.setText(String.format("%d/%d",life2,posion2));
    }

}